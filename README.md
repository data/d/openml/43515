# OpenML dataset: Egg-Producing-Chickens

https://www.openml.org/d/43515

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This data set could be used without permission for purposes of enhancing machine learning and data science. It can easily be adapted for classification, regression and clustering.  At a practical level, such machine learning could be applied to determine which chickens are due for curling or approximating the number of eggs to be harvested 
Content
GallusBreed - breed of chicken such as Buff Orpington chicken
Day - an integer indicating the day on which an observation was made
Age - age of the chicken in weeks
GallusWeight - weight of the chicken in grams
GallusEggColor - color of the eggs 
GallusEggWeight - weight of the eggs in grams 
AmountOfFeed - amount of feed in grams the chicken consumed per day
EggsPerDay - number of eggs a chicken laid on a particular day 
GallusCombType - comb type of a particular chicken
SunLightExposure - number of hours a chicken is exposed to natural light (sunlight) in a day
GallusClass - chicken classes as classified by international Poultry associations
GallusLegShanksColor - color of the legs/feet and shanks on them
GallusBeakColor - color of the chickens beak
GallusEarLobesColor - color of the chicken earlobes 
GallusPlumage - color of the feathers
Acknowledgements
http://www.chickenbreedsoftheworld.com
https://www.thehappychickencoop.com/10-breeds-of-chicken-that-will-lay-lots-of-eggs-for-you
https://www.starmilling.com/poultry-chicken-breeds.php
https://agronomag.com/top-13-best-egg-laying-chicken-breeds
Inspiration
I am very passionate about Data Science, Machine Learning and Artificial Intelligence. My challenge at the moment is the lack of active participation from the African continent. It is for this reason that I am of the view that my contribution of this data set will add to the African contribution to the broader Data Science, Machine Learning and Artificial Intelligence ecosystem

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43515) of an [OpenML dataset](https://www.openml.org/d/43515). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43515/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43515/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43515/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

